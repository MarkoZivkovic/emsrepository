
insert into oz_obrazacmz
select :P_Obracun, kadar, sum( a.zarada) zarada, sum(a. uvecana_zarada) uvecanje, sum(a.varijabila) varijabila,  sum(a.bruto) bruto 
from 
(
select 
s.hr_kadar kadar, 
          -- round(os.iznos) neto,            
            round(os.iznos) - round((os.iznos-ss.osnovnazarada*0.701)-ss.olaksica*0.12005*0.701 - ss.olaksicados*0.12005*0.701) + ROUND( (ss.osnovnazarada * S.STIMULACIJA*0.01*0.701))zarada, 
          -- round((os.iznos-ss.osnovnazarada*0.701)-ss.olaksica*0.12005 - ss.olaksicados*0.12005) uvecana_zarada,
            round((os.iznos-ss.osnovnazarada*0.701)-(ss.olaksica*0.701*0.12005 - ss.olaksicados*0.12005*0.701)-(ss.osnovnazarada * S.STIMULACIJA*0.01*0.701)) uvecana_zarada , 0 varijabila,
            round(ss.iznos) bruto
from    oz_obracun o, oz_sati s, oz_satistavka ss, oz_obracunstavka os, oz_obracunstavkavrsta ov, oz_vrstarada v
where   o.id = s.oz_obracun
and s.id = ss.oz_sati
and ss.oz_vrstarada = v.id
and ss.id = os.oz_satistavka
and os.oz_obracunstavkavrsta = ov.id
and o.id = :P_Obracun
and V.OZ_OBRACUNIZNOSA =   4
and v.vrstaboda = 'T'
and OV.SIFRA = 'ON'
and s.hr_kadar not in (
    select hr_kadar
    from oz_obracunbudzet
    where   oz_obracun = :P_obracun
    )
union all
select 
s.hr_kadar, 
          --  round(os.iznos) neto, 
            round(os.iznos) zarada,  
            0 uvecana_zarada,
            0, 
            round(ss.iznos)
from    oz_obracun o, oz_sati s, oz_satistavka ss, oz_obracunstavka os, oz_obracunstavkavrsta ov, oz_vrstarada v1, oz_obracunnacin n
where   o.id = s.oz_obracun
and s.id = ss.oz_sati
and ss.oz_vrstarada not in (
        select  v.id
        from    oz_vrstarada v
        where v.vrstaboda = 'T'
        and V.OZ_OBRACUNIZNOSA =   4
        )
and SS.OZ_VRSTARADA = v1.id        
and v1.oz_obracunnacin = n.id
and N.NAKNADA = 'N'
and ss.id = os.oz_satistavka
and os.oz_obracunstavkavrsta = ov.id
and o.id = :P_Obracun
and OV.SIFRA = 'ON'
and s.hr_kadar not in (
    select hr_kadar
    from oz_obracunbudzet
    where   oz_obracun = :P_obracun
    )
) a 
--where   a.kadar = hr_kadarPKg.getId('04000')
group by kadar

