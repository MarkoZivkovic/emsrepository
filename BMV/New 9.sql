insert into oz_obrazacmz
select 4003 oz_obracun, t.hr_kadar, sum(t.zarada) zarada, sum(t.uvecanje) uvecanje, sum(t.varijabila) varijabila, sum(t.bruto) bruto
from (
-- UVECANJA EFEKTIVNIH ISPLATA --
select s.hr_kadar,  0 zarada, --sum(os.iznos) neto, round(sum(ss.osnovnazarada*0.701)) oz, 
         (sum(os.iznos) - round(sum(ss.osnovnazarada*0.701))) uvecanje, 0 varijabila, round(sum(ss.iznos-ss.osnovnazarada)) bruto
from    oz_sati s, oz_satistavka ss, oz_obracunstavka os
where   S.ID = ss.oz_sati
and ss.oz_vrstarada in (
    select v.id
    from    oz_vrstarada v
    where V.OZ_OBRACUNIZNOSA = 4
    and V.VRSTABODA = 'T'
    )
and s.oz_obracun = 3981
and ss.id = os.oz_satistavka
and OS.OZ_OBRACUNSTAVKAVRSTA = 81
and s.hr_kadar not in (
    select hr_kadar
    from oz_obracunbudzet
    where   oz_obracun = 103 --:p_obracun
    )
group by s.hr_kadar
union all
-- UVECANJA PROSECNIH ISPLATA --
select  s.hr_kadar,  0 neto,  round((sum(ss.iznos) - sum(ss.koeficijent*SS.SATIRADA*O.BOD*V1.KOEFICIJENT ) )*0.701) uvecanje, 0,  round(sum(ss.iznos) - sum(ss.koeficijent*SS.SATIRADA*O.BOD*V1.KOEFICIJENT )) bruto
from    oz_sati s, oz_satistavka ss, oz_obracunstavka os, oz_vrstarada v1, oz_obracun o
where   S.ID = ss.oz_sati
and o.id = s.oz_obracun
and v1.id = ss.oz_vrstarada
and ss.oz_vrstarada in (
    select v.id
    from    oz_vrstarada v
    where V.OZ_OBRACUNIZNOSA = 4
    and V.VRSTABODA = 'S'
    --and V.MINULIRAD = 'D'
    )
and s.oz_obracun = 3981
--and V1.MINULIRAD = 'D'
and ss.id = os.oz_satistavka
and OS.OZ_OBRACUNSTAVKAVRSTA = 81
and s.hr_kadar not in (
    select hr_kadar
    from oz_obracunbudzet
    where   oz_obracun = 103 --:p_obracun
    )
group by s.hr_kadar
union all
-- ISPLATA TO I RG 
select s.hr_kadar,  round(sum(os.iznos)) neto,  0 uvecanje, 0, round(sum(ss.iznos)) bruto
from    oz_sati s, oz_satistavka ss, oz_obracunstavka os
where   S.ID = ss.oz_sati
and ss.oz_vrstarada  in (
    select v.id
    from    oz_vrstarada v
    where V.sifra in ('004', '005', '006')
    )
and s.oz_obracun = 3981
and ss.id = os.oz_satistavka
and OS.OZ_OBRACUNSTAVKAVRSTA = 81
and s.hr_kadar not in (
    select hr_kadar
    from oz_obracunbudzet
    where   oz_obracun = 103 --:p_obracun
    )
group by s.hr_kadar
-- ISPLATA PROMRNLJIVA U ZAVISNOSTI OD SATI
union all
select  s.hr_kadar,  sum(os.iznos) neto,  0 uvecanje, 0,  sum(ss.iznos) bruto
from    oz_sati s, oz_satistavka ss, oz_obracunstavka os, oz_vrstarada v1, oz_obracun o
where   S.ID = ss.oz_sati
and o.id = s.oz_obracun
and v1.id = ss.oz_vrstarada
and ss.oz_vrstarada in (
    select v.id
    from    oz_vrstarada v
    where V.sifra = '110'
    )
and s.oz_obracun = 4003
--and V1.MINULIRAD = 'D'
and ss.id = os.oz_satistavka
and OS.OZ_OBRACUNSTAVKAVRSTA = 81
--and s.hr_kadar  = hr_kadarPkg.getId('04000')
    and s.hr_kadar not in (
    select hr_kadar
    from oz_obracunbudzet
    where   oz_obracun = 103 --:p_obracun
    )
group by s.hr_kadar
) t
group by t.hr_kadar
