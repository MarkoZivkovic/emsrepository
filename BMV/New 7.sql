insert into oz_obrazacmz

select 4003, t.hr_kadar, 
            --k.platnibroj,  HR_KADARPKG.GETIMECYR(t.hr_kadar) ime, HR_KADARPKG.GETPREZIMECYR(t.hr_kadar) prezime,
            --k.jmbg, 
           --MP.NOVA_SIFRA radnomesto,
           --MS.NOVA_SIFRA skolskasprema,             
            round(sum(zarada)) -round(sum(t.uvecana)) zarada, 
            round(sum(t.uvecana)) uvecana, 
            round(sum(t.varijabilni)) varijabilni,
           -- round(sum(t.neto)) neto, 
            round(sum(t.bruto)) bruto
from (   
select  s.hr_kadar,
        round(sum(os.iznos)) zarada, 0 uvecana, 0 varijabilni, round(sum(os.iznos)) neto, round(sum(ss.iznos)) bruto, s.oz_obracun
from    oz_obracun o, oz_sati s, oz_satistavka ss, oz_vrstarada v, oz_obracunstavka os
where   o.id  = s.oz_obracun
and s.id = ss.oz_sati
and ss.oz_vrstarada = v.id
and ss.id = os.oz_satistavka
and OS.OZ_OBRACUNSTAVKAVRSTA = 81
and o.id = 4003 --:k_obracun
and s.hr_kadar in (
    select hr_kadar
    from oz_obracunbudzet
    where   oz_obracun = 103 --:p_obracun
    )
group by s.hr_kadar, s.oz_obracun
union all
select s.hr_kadar, 0, round(sum(SS.MINULIRAD)*0.701),  0  varijabilni, 0 neto, 0 bruto, s.oz_obracun
from    oz_obracun o, oz_sati s, oz_satistavka ss, oz_vrstarada v, oz_obracunstavka os
where   o.id  = s.oz_obracun
and s.id = ss.oz_sati
and ss.oz_vrstarada = v.id
and ss.id = os.oz_satistavka
and OS.OZ_OBRACUNSTAVKAVRSTA = 81
and o.id = 3981
and s.hr_kadar in (
    select hr_kadar
    from oz_obracunbudzet
    where   oz_obracun = 103 --:p_obracun
    )
--and v.sifra in ('004', '006')
group by s.hr_kadar, s.oz_obracun
) t, hr_kadar k,  hr_mapsis mp,  hr_mapskola ms, hr_zaposlenje z, hr_sistematizacija s, hr_skolskasprema sp
where   k.id = t.hr_kadar
and k.id = z.hr_kadar
and z.hr_sistematizacija = s.id
and z.datumod <= OZ_OBRACUNPKG.GETKRAJMESECA(103) --:p_obracun)
and z.datumdo >= OZ_OBRACUNPKG.GETKRAJMESECA(103) --:p_obracun)
           and  S.SIFRA=mp.sifra(+)
            and SP.SIFRA=MS.SIFRA(+)
and z.hr_skolskasprema = sp.id            
group by t.hr_kadar --, t.oz_obracun, k.jmbg, k.platnibroj,  MP.NOVA_SIFRA, MS.NOVA_SIFRA
order by 4, 3
