
insert into bmv
select s.hr_kadar, 
            round(os.iznos) neto,            
            round(os.iznos) - round((os.iznos-ss.osnovnazarada*0.701)-ss.olaksica*0.12005 - ss.olaksicados*0.12005) zarada, 
            round((os.iznos-ss.osnovnazarada*0.701)-ss.olaksica*0.12005 - ss.olaksicados*0.12005) uvecana_zarada 
from    oz_obracun o, oz_sati s, oz_satistavka ss, oz_obracunstavka os, oz_obracunstavkavrsta ov, oz_vrstarada v
where   o.id = s.oz_obracun
and s.id = ss.oz_sati
and ss.oz_vrstarada = v.id
and ss.id = os.oz_satistavka
and os.oz_obracunstavkavrsta = ov.id
and o.id = :uObracun
and v.id not in (
    select v2.id
    from    oz_vrstarada v2
        where (v.vrstaboda = 'T' 
        and V2.OZ_OBRACUNIZNOSA =   4)
        or
        (v2.vrstaboda != 'T' and v2.sifra='005')
        )
and OV.SIFRA = 'ON';

insert into bmv
select s.hr_kadar, 
            round(os.iznos) neto, 
            round(os.iznos) zarada,  
            0 uvecana_zarada        
from    oz_obracun o, oz_sati s, oz_satistavka ss, oz_obracunstavka os, oz_obracunstavkavrsta ov, oz_vrstarada v1, oz_obracunnacin n
where   o.id = s.oz_obracun
and s.id = ss.oz_sati
and v1.id  not in (
    select v2.id
    from    oz_vrstarada v2    
        where (v2.vrstaboda = 'T' 
        and V2.OZ_OBRACUNIZNOSA =   4)
        or
        (v2.vrstaboda != 'T' and v2.sifra='005')
        )
and SS.OZ_VRSTARADA = v1.id        
and v1.oz_obracunnacin = n.id
and N.NAKNADA = 'N'
and ss.id = os.oz_satistavka
and os.oz_obracunstavkavrsta = ov.id
and o.id = :uObracun
and OV.SIFRA = 'ON';


drop table bmv

create table bmv as
select hr_kadarPkg.getPlatnibroj(s.hr_kadar) mbr, o.zs_mesec, sum(SS.SATIRADA) satirada, sum(ss.iznos) iznos, 0 sati
from    oz_obracun o, oz_sati s, oz_satistavka ss, oz_vrstarada v
where   o.id =s.oz_obracun
and s.id = ss.oz_sati
and ss.oz_vrstarada = v.id
and O.ZS_GODINA = 2012
and O.OZ_OBRACUNTIP = 1
and O.OZ_OBRACUNVRSTA = 1
and O.ZS_MESEC between 1 and 6
and v.sifra in ('220','221', '222', '922', '9222',  '9220', '925', '926' )
group by s.hr_kadar, o.zs_mesec
union all
select mbr, mesec, 0 satirada, 0 iznos, sati*8 sati
from    A000_ODMOR_STARA_1
WHERE   SATI  < 25
UNION ALL
select mbr, mesec, 0 satirada, 0 iznos, satI/10*8 sati
from    A000_ODMOR_STARA_1
WHERE   SATI >25

select mbr, mesec, 0 satirada, 0 iznos, satI  --/10*8 sati
from    A000_ODMOR_STARA_1
WHERE   MBR in ( 
select mbr
from    (
select mbr, mesec
from    A000_ODMOR_STARA_1
GROUP BY mbr, mesec
having count(*) > 1
) t
)

select mbr, zs_mesec, sum(satirada) satirada, sum(iznos) iznos, sum(sati) sati
from   bmv
group by  mbr, zs_mesec
having sum(satirada)  = sum(sati)

select mbr, zs_mesec, sum(satirada) satirada, sum(iznos) iznos, sum(sati) sati
from   bmv
group by  mbr, zs_mesec
having sum(satirada)  != sum(sati)
and sum(sati) != 0


select mbr, sum(satirada) sati_LD, sum(iznos) iznos_LD, sum(sati) sati, EMSUTIL.ZAOKRUZIIZNOS05((sum(sati)/decode(sum(satirada), 0,  sum(sati), sum(satirada)))*sum(iznos)) trazeni_bruto
from   bmv
group by  mbr
having sum(sati)  != 0

having sum(satirada)  = sum(sati)