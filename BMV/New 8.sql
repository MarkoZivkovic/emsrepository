    select hr_kadar, hr_kadarPkg.getPunoime(hr_kadar)
    from oz_obracunbudzet
    where   oz_obracun = :p_obracun

insert into oz_obrazacmz
select t.oz_obracun, t.hr_kadar, 
        --k.platnibroj,  HR_KADARPKG.GETIMECYR(t.hr_kadar) ime, HR_KADARPKG.GETPREZIMECYR(t.hr_kadar) prezime,
            --k.jmbg, 
           --MP.NOVA_SIFRA radnomesto,
           --MS.NOVA_SIFRA skolskasprema,             
            sum(zarada) zarada, 
            sum(t.uvecana) uvecana, 
            sum(t.varijabilni) varijabilni,
            --sum(t.neto) neto, 
            sum(t.bruto) bruto
from (            
select s.hr_kadar,  round(165995.50) zarada, round(sum(os.iznos))-round(165995.50) uvecana, 0 varijabilni, round(sum(os.iznos)) neto,  round(sum(ss.iznos)) bruto, s.oz_obracun
from    oz_obracun o, oz_sati s, oz_satistavka ss, oz_obracunstavka os
where   o.id  = s.oz_obracun
and s.id = ss.oz_sati
and ss.id = os.oz_satistavka
and OS.OZ_OBRACUNSTAVKAVRSTA = 81
and o.id = 103 --:p_obracun
and s.hr_kadar in (
    select hr_kadar
    from oz_obracunbudzet
    where   oz_obracun = 103 --:p_obracun
    )
group by s.oz_obracun, s.hr_kadar
)  t, hr_kadar k,  hr_mapsis mp,  hr_mapskola ms, hr_zaposlenje z, hr_sistematizacija s, hr_skolskasprema sp
where   k.id = t.hr_kadar
and k.id = z.hr_kadar
and z.hr_sistematizacija = s.id
and z.datumod <= OZ_OBRACUNPKG.GETKRAJMESECA(103) --:p_obracun)
and z.datumdo >= OZ_OBRACUNPKG.GETKRAJMESECA(103) --:p_obracun)
           and  S.SIFRA=mp.sifra(+)
            and SP.SIFRA=MS.SIFRA(+)
and z.hr_skolskasprema = sp.id            
group by t.oz_obracun, t.hr_kadar --, k.jmbg, k.platnibroj,  MP.NOVA_SIFRA, MS.NOVA_SIFRA
order by 4, 3           
              
having sum(t.neto) != 0


select s.hr_kadar,  165995.50 zarada, sum(os.iznos)-165995.50 uvecana, 0 varijabilni, 0 neto,  sum(ss.iznos) bruto 
from    oz_obracun o, oz_sati s, oz_satistavka ss, oz_obracunstavka os
where   o.id  = s.oz_obracun
and s.id = ss.oz_sati
and ss.id = os.oz_satistavka
and OS.OZ_OBRACUNSTAVKAVRSTA = 81
and o.id = :p_obracun
and s.hr_kadar in (
    select hr_kadar
    from oz_obracunbudzet
    where   oz_obracun = :p_obracun
    )
group by s.hr_kadar