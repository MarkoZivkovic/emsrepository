drop table bmv;

create table bmv
as
select s.hr_kadar, 
            round(os.iznos) neto,            
            round(os.iznos) - round((os.iznos-ss.osnovnazarada*0.701)-ss.olaksica*0.12005 - ss.olaksicados*0.12005) zarada, 
            round((os.iznos-ss.osnovnazarada*0.701)-ss.olaksica*0.12005 - ss.olaksicados*0.12005) uvecana_zarada ,
            0 st_iznos,
              S.STIMULACIJA
from    oz_obracun o, oz_sati s, oz_satistavka ss, oz_obracunstavka os, oz_obracunstavkavrsta ov, oz_vrstarada v
where   o.id = s.oz_obracun
and s.id = ss.oz_sati
and ss.oz_vrstarada = v.id
and ss.id = os.oz_satistavka
and os.oz_obracunstavkavrsta = ov.id
and V.OZ_OBRACUNIZNOSA =   4
and v.vrstaboda = 'T'
and OV.SIFRA = 'ON'
and 1=0;

-- REDOVAN RAD --

insert into bmv
select  s.hr_kadar, 
            round(os.iznos) neto,            
            round(os.iznos) - round((os.iznos-ss.osnovnazarada*0.701)-ss.olaksica*0.12005 - ss.olaksicados*0.12005) zarada, 
            round((os.iznos-ss.osnovnazarada*0.701)-ss.olaksica*0.12005 - ss.olaksicados*0.12005) uvecana_zarada , ss.osnovnazarada * S.STIMULACIJA*0.01*0.701 st_iznos, S.STIMULACIJA
from    oz_obracun o, oz_sati s, oz_satistavka ss, oz_obracunstavka os, oz_obracunstavkavrsta ov, oz_vrstarada v
where   o.id = s.oz_obracun
and s.id = ss.oz_sati
and ss.oz_vrstarada = v.id
and ss.id = os.oz_satistavka
and os.oz_obracunstavkavrsta = ov.id
and o.id = :uObracun
and V.OZ_OBRACUNIZNOSA =   4
and v.vrstaboda = 'T'
and OV.SIFRA = 'ON';

-- PRIMANJA NA OSNOVU PROSEKA

insert into bmv
select s.hr_kadar, 
            round(os.iznos) neto, 
            round(os.iznos)-round((SS.SATIRADA*OP.MINULI1*V1.KOEFICIJENT)*0.701) zarada,  
            round((SS.SATIRADA*OP.MINULI1*V1.KOEFICIJENT)*0.701) uvecana_zarada, 0 , 0       
from    oz_obracun o, oz_sati s, oz_satistavka ss, oz_obracunstavka os, oz_obracunstavkavrsta ov, oz_vrstarada v1, oz_obracunnacin n, oz_satiprosek op
where   o.id = s.oz_obracun
and s.id = ss.oz_sati
and ss.oz_vrstarada  in (
        select  v.id
        from    oz_vrstarada v
        where v.vrstaboda = 'S'
        and V.OZ_OBRACUNIZNOSA =   4
        )
and SS.OZ_VRSTARADA = v1.id        
and v1.oz_obracunnacin = n.id
and N.NAKNADA = 'N'
and ss.id = os.oz_satistavka
and os.oz_obracunstavkavrsta = ov.id
and o.id = :uObracun
and OV.SIFRA = 'ON'
--and s.hr_kadar = hr_kadarPkg.getId('04145')
and s.id = op.oz_sati;


-- OSTALA PRIMANJA

insert into bmv
select s.hr_kadar, 
            round(os.iznos) neto, 
            round(os.iznos) zarada,  
            0 uvecana_zarada, 0 , 0       
from    oz_obracun o, oz_sati s, oz_satistavka ss, oz_obracunstavka os, oz_obracunstavkavrsta ov, oz_vrstarada v1, oz_obracunnacin n
where   o.id = s.oz_obracun
and s.id = ss.oz_sati
and ss.oz_vrstarada not in (
        select  v.id
        from    oz_vrstarada v
        where v.vrstaboda = 'T'
        and V.OZ_OBRACUNIZNOSA =   4
        union all
        select  v.id
        from    oz_vrstarada v
        where v.vrstaboda = 'S'
        and V.OZ_OBRACUNIZNOSA =   4        
        )
and SS.OZ_VRSTARADA = v1.id        
and v1.oz_obracunnacin = n.id
and N.NAKNADA = 'N'
and ss.id = os.oz_satistavka
and os.oz_obracunstavkavrsta = ov.id
and o.id = :uObracun
and OV.SIFRA = 'ON';

/*
select hr_kadarPkg.getplatnibroj(hr_kadar) mbr, sum(neto) ONT, sum(uvecana_zarada) min, sum(zarada) RAZ, 
        165995.50 max, 165995.50+sum(uvecana_zarada) ww, sum(st_iznos) st, sum(stimulacija)
from    bmv
where hr_kadarPkg.getplatnibroj(hr_kadar)  in ('04113', '04130', '04583', '04156', '04418')
group by hr_kadar;
*/

drop  table bmv3;

create table bmv3
as

select hr_kadar, hr_kadarPkg.getplatnibroj(hr_kadar) mbr,  hr_kadarPkg.getPunoIme(hr_kadar) ime, sum(neto) ONT, sum(uvecana_zarada) min, sum(zarada) RAZ, 
        165995.50 max, 165995.50+sum(uvecana_zarada) ww, sum(zarada)+sum(st_iznos) to_je_to, sum(st_iznos) st, max(stimulacija) stim
from    bmv
--where hr_kadarPkg.getplatnibroj(hr_kadar) not in ('03299', '04581', '04145', '04580')
group by hr_kadar
having (sum(zarada)+sum(st_iznos) ) > 165995.50
order by 9 desc;

select *
from  bmv3

from    bmv
where hr_kadarPkg.getplatnibroj(hr_kadar) not in ('03299', '04581', '04145', '04580')
group by hr_kadar
having (sum(zarada)+sum(st_iznos) )> 165000;


select t.mbr, t.ime, sum(t.iznos)iznos
from    (
select hr_kadarPkg.getplatnibroj(hr_kadar) mbr,  hr_kadarPkg.getPunoIme(hr_kadar) ime, sum(iznos) iznos
from    oz_obracun o, oz_sati s, oz_satistavka ss
where   o.id = s.oz_obracun
and s.id = ss.oz_sati
and s.hr_kadar in (
    select hr_kadar from bmv3
    )
and o.id = 103
group by hr_kadar
union all
select hr_kadarPkg.getplatnibroj(hr_kadar) mbr,  hr_kadarPkg.getPunoIme(hr_kadar) ime, -sum(iznos)
from    oz_obracun o, oz_sati s, oz_satistavka ss
where   o.id = s.oz_obracun
and s.id = ss.oz_sati
and s.hr_kadar in (
    select hr_kadar from bmv3
    )
and o.id = 4003
group by hr_kadar
) t    
group by t.mbr, t.ime